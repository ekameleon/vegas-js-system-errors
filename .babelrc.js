const presets =
[
  [ "@babel/preset-env" , { "modules" : "auto" } ]
];

const plugins =
[
  [ "@babel/plugin-transform-runtime" , { helpers:false , useESModules:true } ],
  '@babel/plugin-proposal-class-properties'
];

module.exports = { presets, plugins };