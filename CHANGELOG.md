# VEGAS JS System Errors - OpenSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.2] - 2020-03-17
### Changed
- Fix the vegas-js-core version 1.0.19

## [1.0.1] - 2018-11-03
### Changed
- Fix the vegas-js-core version : 1.0.5

## [1.0.0] - 2018-10-28
### Added
- First version

