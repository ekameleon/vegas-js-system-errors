"use strict" ;

import babel   from 'rollup-plugin-babel'
import cleanup from 'rollup-plugin-cleanup'
import license from 'rollup-plugin-license'
import replace from 'rollup-plugin-replace'
import resolve from 'rollup-plugin-node-resolve'
import uglify  from 'rollup-plugin-uglify-es'

import pkg from './package.json'

import setting from './build/config.json'

let mode = 'dev' ;
let file = setting.output + setting.file ;

try
{
    switch( process.env.MODE )
    {
        case 'prod' :
        {
            mode = 'prod' ;
            file += '.min.js' ;
            break ;
        }
        default :
        {
            mode = 'dev' ;
            file += '.js' ;
        }
    }
}
catch (e) {}

const plugins =
[
    resolve(),
    babel
    ({
        exclude        : 'node_modules/**' ,
        runtimeHelpers : true
    }),
    replace
    ({
        delimiters : [ '<@' , '@>' ] ,
        values     :
        {
            NAME        : pkg.name ,
            DESCRIPTION : pkg.description ,
            HOMEPAGE    : pkg.homepage ,
            LICENSE     : pkg.license ,
            VERSION     : pkg.version
        }
    }),
    cleanup()
];

if( mode === 'prod' )
{
    plugins.push( uglify() )
}

plugins.push( license({ banner : setting.header }) ) ;

export default
{
    input  : setting.entry ,
    output :
    {
        name      : setting.bundle ,
        file      : file  ,
        format    : 'umd' ,
        sourcemap : setting.sourcemap && (mode === 'dev') ,
        strict    : true
    },
    watch :
    {
        exclude : 'node_modules/**'
    },
    plugins : plugins
} ;
