"use strict" ;

import ConcurrencyError    from './src/ConcurrencyError'
import InvalidChannelError from './src/InvalidChannelError'
import InvalidFilterError  from './src/InvalidFilterError'
import NonUniqueKeyError   from './src/NonUniqueKeyError'
import NoSuchElementError  from './src/NoSuchElementError'

/**
 * The system-errors library is the root error library for the <strong>VEGAS JS</strong> framework.
 * @summary The system-errors library is the root package for the <strong>VEGAS JS</strong> framework.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace system.errors
 * @version 1.0.0
 * @since 1.0.0
 */
export default
{
    ConcurrencyError ,
    InvalidChannelError,
    InvalidFilterError ,
    NonUniqueKeyError ,
    NoSuchElementError ,
} ;