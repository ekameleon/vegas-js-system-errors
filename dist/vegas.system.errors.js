/**
 * A specialized set of error classes - version: 1.0.2 - license: MPL 2.0/GPL 2.0+/LGPL 2.1+ - Follow me on Twitter! @ekameleon
 */

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global.vegas_system_errors = factory());
}(this, (function () { 'use strict';

  function _defineProperty(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
    } else {
      obj[key] = value;
    }

    return obj;
  }

  function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i] != null ? arguments[i] : {};
      var ownKeys = Object.keys(source);

      if (typeof Object.getOwnPropertySymbols === 'function') {
        ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) {
          return Object.getOwnPropertyDescriptor(source, sym).enumerable;
        }));
      }

      ownKeys.forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    }

    return target;
  }

  function ConcurrencyError(message, fileName, lineNumber) {
    this.name = 'ConcurrencyError';
    this.message = message || 'concurrency error';
    this.fileName = fileName;
    this.lineNumber = lineNumber;
    this.stack = new Error().stack;
  }
  ConcurrencyError.prototype = Object.create(Error.prototype);
  ConcurrencyError.prototype.constructor = ConcurrencyError;

  function InvalidChannelError(message, fileName, lineNumber) {
    this.name = 'InvalidChannelError';
    this.message = message || 'invalid channel error';
    this.fileName = fileName;
    this.lineNumber = lineNumber;
    this.stack = new Error().stack;
  }
  InvalidChannelError.prototype = Object.create(Error.prototype);
  InvalidChannelError.prototype.constructor = InvalidChannelError;

  function InvalidFilterError(message, fileName, lineNumber) {
    this.name = 'InvalidFilterError';
    this.message = message || 'invalid filter error';
    this.fileName = fileName;
    this.lineNumber = lineNumber;
    this.stack = new Error().stack;
  }
  InvalidFilterError.prototype = Object.create(Error.prototype);
  InvalidFilterError.prototype.constructor = InvalidFilterError;

  function fastformat( pattern , ...args )
  {
      if( (pattern === null) || !(pattern instanceof String || typeof(pattern) === 'string' ) )
      {
          return "" ;
      }
      if( args.length > 0 )
      {
          args = [].concat.apply( [] , args ) ;
          let len  = args.length ;
          for( let i = 0 ; i < len ; i++ )
          {
              pattern = pattern.replace( new RegExp( "\\{" + i + "\\}" , "g" ), args[i] );
          }
      }
      return pattern;
  }

  function NonUniqueKeyError(key, pattern, fileName, lineNumber) {
    this.name = 'NonUniqueKeyError';
    this.key = key;
    this.pattern = pattern || NonUniqueKeyError.PATTERN;
    this.message = fastformat(this.pattern, key);
    this.fileName = fileName;
    this.lineNumber = lineNumber;
    this.stack = new Error().stack;
  }
  NonUniqueKeyError.PATTERN = "attempting to insert the key '{0}'";
  NonUniqueKeyError.prototype = Object.create(Error.prototype);
  NonUniqueKeyError.prototype.constructor = NonUniqueKeyError;

  function NoSuchElementError(message, fileName, lineNumber) {
    this.name = 'NoSuchElementError';
    this.message = message || 'no such element error';
    this.fileName = fileName;
    this.lineNumber = lineNumber;
    this.stack = new Error().stack;
  }
  NoSuchElementError.prototype = Object.create(Error.prototype);
  NoSuchElementError.prototype.constructor = NoSuchElementError;

  /**
   * The system-errors library is the root error library for the <strong>VEGAS JS</strong> framework.
   * @summary The system-errors library is the root package for the <strong>VEGAS JS</strong> framework.
   * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
   * @author Marc Alcaraz <ekameleon@gmail.com>
   * @namespace system.errors
   * @version 1.0.0
   * @since 1.0.0
   */
  var data = {
    ConcurrencyError: ConcurrencyError,
    InvalidChannelError: InvalidChannelError,
    InvalidFilterError: InvalidFilterError,
    NonUniqueKeyError: NonUniqueKeyError,
    NoSuchElementError: NoSuchElementError
  };

  var skip = false ;
  function sayHello( name = '' , version = '' , link = '' )
  {
      if( skip )
      {
          return ;
      }
      try
      {
          if ( navigator && navigator.userAgent && navigator.userAgent.toLowerCase().indexOf('chrome') > -1)
          {
              const args = [
                  `\n %c %c %c ${name} ${version} %c %c ${link} %c %c\n\n`,
                  'background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #000000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'color: #F7FF3C; background: #ff0000; padding:5px 0;',
                  'background: #AA0000; padding:5px 0;',
                  'background: #ff0000; padding:5px 0;',
              ];
              window.console.log.apply( console , args );
          }
          else if (window.console)
          {
              window.console.log(`${name} ${version} - ${link}`);
          }
      }
      catch( error )
      {
      }
  }
  function skipHello()
  {
      skip = true ;
  }

  var metas = Object.defineProperties({}, {
    name: {
      enumerable: true,
      value: 'vegas-js-system-errors'
    },
    description: {
      enumerable: true,
      value: 'A specialized set of error classes'
    },
    version: {
      enumerable: true,
      value: '1.0.2'
    },
    license: {
      enumerable: true,
      value: "MPL-2.0 OR GPL-2.0+ OR LGPL-2.1+"
    },
    url: {
      enumerable: true,
      value: 'https://bitbucket.org/ekameleon/vegas-js-system-errors'
    }
  });
  var bundle = _objectSpread({
    metas: metas,
    sayHello: sayHello,
    skipHello: skipHello
  }, data);
  try {
    if (window) {
      window.addEventListener('load', function load() {
        window.removeEventListener("load", load, false);
        sayHello(metas.name, metas.version, metas.url);
      }, false);
    }
  } catch (ignored) {}

  return bundle;

})));
//# sourceMappingURL=vegas.system.errors.js.map
