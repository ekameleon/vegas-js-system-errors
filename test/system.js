'use strict' ;

import chai from 'chai' ;

import ConcurrencyError from '../src/ConcurrencyError'

const assert = chai.assert ;

describe( 'system' , () =>
{
    it('Enum is not null', () => { assert.isNotNull( ConcurrencyError ); });
});
